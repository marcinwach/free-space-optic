package com.example.freespaceoptic;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.view.View;
import android.widget.Button;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

public class Sender extends Thread {

    public TransmitterUtils tu;
    CameraManager cameraManager;
    String cameraId;
    String text;
    public boolean stopSending = false;

    public Sender(TransmitterUtils tu, CameraManager cameraManager, String cameraId, String text) {
        this.tu = tu;
        this.cameraManager = cameraManager;
        this.cameraId = cameraId;
        this.text = text;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void run() {
        send(this.text);
    }

    /**
     * Iterate through characters, encode it to Morse sequence and call
     *  functions responsible for flashing light.
     *
     * @param text  text to be send
     */
    public void send(String text){
        text += "O";
        if (text == null) return;
        else {
            text = text.toUpperCase();
            for (int i = 0; i < text.length(); i++) {
                if (stopSending) {
                    break;
                }
                //Log.i("MyLog", tu.library.get(Character.toString(text.charAt(i))));
                if (text.charAt(i) == ' '){
                    try {
                        Thread.sleep(this.tu.gapBetweenWordsTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else{
                    // call function
                    String morse = this.tu.library.get(Character.toString(text.charAt(i)));
                    processMorse(morse);
                    try {
                        Thread.sleep(this.tu.gapBetweenCharacterTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    /**
     * Flashes light for duration of Morse dot symbol
     */
    public void dotLight(){
        try {
            this.cameraManager.setTorchMode(this.cameraId, true);
            try {
                Thread.sleep(this.tu.dotTime);
                this.cameraManager.setTorchMode(this.cameraId, false);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Flashes light for duration of Morse dash symbol
     */
    public void dashLight(){
        try {
            this.cameraManager.setTorchMode(this.cameraId, true);
            try {
                Thread.sleep(this.tu.dashTime);
                this.cameraManager.setTorchMode(this.cameraId, false);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process sequence of morse symbols (dot and dashes)
     * If detected symbol is dot call method dotLight
     * If detected symbol is dash call method dashLight
     * After proceeding given sequence it sleep for duration of dot.
     *
     * @param morse  sequence of morse (dot and dashes)
     */
    public void processMorse(String morse){
        for (int i = 0; i < morse.length(); i++) {
            if ( morse.charAt(i) == '.'){
                dotLight();
            }else{
                dashLight();
            }
            try {
                Thread.sleep(this.tu.dotTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}

package com.example.freespaceoptic;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class TransmitterUtils {
    public  long dotTime = 300;
    public  long dashTime = 3 * dotTime;
    public long gapBetweenMorseSigns = 300;
    public long gapBetweenWordsTime = 7 * dotTime - gapBetweenMorseSigns;

    public long gapBetweenCharacterTime = 3 * dotTime - gapBetweenMorseSigns;
    public Map<String, String> library;
    public Map<String, String> invertedLibrary;
    public Map<String, String> librarySpaces;

    /**
     * In constructor there is created object which stores mapping morse symbols sequences into letters
     * and vice versa.
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public TransmitterUtils(){
        Map<String, String> library = new HashMap<String, String>();
        library.put(".-", "A");
        library.put("-...", "B");
        library.put("-.-.", "C");
        library.put("-..", "D");
        library.put(".", "E");
        library.put("..-.", "F");
        library.put("--.", "G");
        library.put("....", "H");
        library.put("..", "I");
        library.put(".---", "J");
        library.put("-.-", "K");
        library.put(".-..", "L");
        library.put("--", "M");
        library.put("-.", "N");
        library.put("---", "O");
        library.put(".--.", "P");
        library.put("--.-", "Q");
        library.put(".-.", "R");
        library.put("...", "S");
        library.put("-", "T");
        library.put("..-", "U");
        library.put("...-", "V");
        library.put(".--", "W");
        library.put("-..-", "X");
        library.put("-.--", "Y");
        library.put("--..", "Z");
        library.put(".----", "1");
        library.put("..---", "2");
        library.put("...--", "3");
        library.put("....-", "4");
        library.put(".....", "5");
        library.put("-....", "6");
        library.put("--...", "7");
        library.put("---..", "8");
        library.put("----.", "9");
        library.put("-----", "0");


        this.invertedLibrary = library;
        Map<String, String> swapped = library.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
        this.library = swapped;

        librarySpaces = new HashMap<String, String>();
        librarySpaces.put("ls", "");
        librarySpaces.put("ws", " ");
        librarySpaces.put("N", "");
        librarySpaces.put("", " ");

    }





}

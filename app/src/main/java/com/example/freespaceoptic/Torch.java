package com.example.freespaceoptic;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.freespaceoptic.R;

public class Torch extends AppCompatActivity {

    public TransmitterUtils tu;

    Button btnFlashLight, btnBlinkFlashLight;
    private static final int CAMERA_REQUEST = 123;
    boolean hasCameraFlash = false;
    CameraManager cameraManager;
    String cameraId;
    public Sender sender;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.torch_activity);

        tu = new TransmitterUtils();
        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            cameraId = cameraManager.getCameraIdList()[0];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        ActivityCompat.requestPermissions(Torch.this,
                new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);

        sender = new Sender(tu, cameraManager, cameraId, GetTextToBeSend());

        Button mClickButton1 = (Button)findViewById(R.id.detection_mode_btn);
        mClickButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sender.stopSending = true;
                Intent myIntent = new Intent(Torch.this, MainActivity.class);
                Torch.this.startActivity(myIntent);
            }
        });

        hasCameraFlash = getPackageManager().
                hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        btnBlinkFlashLight = findViewById(R.id.btnBlinkFlashLight);



        btnBlinkFlashLight.setOnClickListener(new View.OnClickListener() {
            /**
             * Start sending text using light
             * @param view
             */
            @Override
            public void onClick(View view) {
                if (sender.isAlive()){
                    sender.stopSending = true;
                    sender.interrupt();
                }
                sender = new Sender(tu, cameraManager, cameraId, GetTextToBeSend());
                sender.stopSending = false;
                sender.start();
            }
        });


    }

    /**
     * Set camera light off
     */
    private void flashLightOn() {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        try {
            String cameraId = cameraManager.getCameraIdList()[0];
            cameraManager.setTorchMode(cameraId, true);
        } catch (CameraAccessException e) {
        }
    }

    /**
     * Set camera light on.
     */
    private void flashLightOff() {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraId = cameraManager.getCameraIdList()[0];
            cameraManager.setTorchMode(cameraId, false);
        } catch (CameraAccessException e) {
        }
    }

    /**
     * Detects if device is equipped with camera and if so it grants privilleges to use it.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    hasCameraFlash = getPackageManager().
                            hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
                } else {
                    btnFlashLight.setEnabled(false);
                    btnBlinkFlashLight.setEnabled(false);
                    Toast.makeText(Torch.this, "Permission Denied for the Camera", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /**
     * Function get text to be send from TextView
     *
     * @return  test which will be send to receiver or null if there is not valid text to be send
     */
    public String GetTextToBeSend(){

        EditText textToBeSent = (EditText)findViewById(R.id.textToBeSent);
        String text = textToBeSent.getText().toString();
        if (text.trim().isEmpty()){
            Toast.makeText(Torch.this, "You must provide any text to be sent",Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(Torch.this, "Sending " + text.trim(),Toast.LENGTH_SHORT).show();
            return text.trim();
        }

        return  null;
    }

}

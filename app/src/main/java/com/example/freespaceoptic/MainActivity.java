package com.example.freespaceoptic;

import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
// ===
//import java.util.List;
//import java.util.ArrayList;
// ===

public class MainActivity extends AppCompatActivity {

    private SensorManager sensorManager;
    private Sensor lightSensor;
    private SensorEventListener lightEventListener;
    private View root;
    private float maxValue;
    // ===
    // Detecting light level
    float detectLightLevel = 500;
    // for edge (light is detected)
    long edgeTimeStart = System.currentTimeMillis();
    long edgeTimeEnd = System.currentTimeMillis();
    long edgeTime = 0;
    // for no edge (light is not detected)
    long noEdgeTimeStart = System.currentTimeMillis();
    long noEdgeTimeEnd = System.currentTimeMillis();
    long noEdgeTime = 0;
    boolean isEdge = false;
    TransmitterUtils tu;
    String buffer = "";  // buffer looks like: '.-ls.-.-ws.-ls.-' == 'aaa aa'
    String readyText = "";
    public boolean started = false;

    // ===
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        root = findViewById(R.id.toolbar);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        Button mClickButton1 = (Button)findViewById(R.id.torch_mode_btn);
        tu = new TransmitterUtils();
        mClickButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, Torch.class);
                MainActivity.this.startActivity(myIntent);
            }
        });


        if (lightSensor == null) {
            Toast.makeText(this, "The device has no light sensor !", Toast.LENGTH_SHORT).show();
            finish();
        }

        // max value for light sensor
        maxValue = lightSensor.getMaximumRange();
        lightEventListener = new SensorEventListener() {
            /**
             * Automatically called after change in illumination detection.
             * If illumination level exceed threshold, function starts to measure duration of light.
             * If illumination level is below threshold, function starts to measure duration of lack of light
             * After duration is measured, time intervals are encoded into dots and dashes.
             * Next dots and dashes are stored in buffer.
             * After end of letter is detected, characters in a buffer are converted to the corresponding letter
             * and added to TextView on GUI.
             *
             * @param sensorEvent
             */
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                float value = sensorEvent.values[0];

                TextView tx = (TextView) findViewById(R.id.mt);
                tx.setTextColor(Color.BLACK);

                // ===
                String character = "";
                // Measure edge and no edge times
                if (value > detectLightLevel && !isEdge) {
                    edgeTimeStart = System.currentTimeMillis();
                    noEdgeTimeEnd = edgeTimeStart;
                    isEdge = true;

                    noEdgeTime = noEdgeTimeEnd - noEdgeTimeStart;
                    character = detectSpace(noEdgeTime);
                    if (character.equals("ls")){
                        readyText += tu.invertedLibrary.get(buffer);
                        //readyText += buffer;
                        buffer = "";
                    }
                    else if( character.equals("ws")){
                        //readyText += buffer;
                        readyText += tu.invertedLibrary.get(buffer);
                        readyText += " ";
                        buffer = "";
                    }

                    //buffer += character;
                } else if (value < detectLightLevel && isEdge) {
                    edgeTimeEnd = System.currentTimeMillis();
                    noEdgeTimeStart = edgeTimeEnd;
                    isEdge = false;

                    edgeTime = edgeTimeEnd - edgeTimeStart;
                    character = detectSymbol(edgeTime);
                    buffer += character;
                }

                tx.setText(readyText);
                int newValue = (int) (255f * value / maxValue);
                root.setBackgroundColor(Color.rgb(newValue, newValue, newValue));
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(lightEventListener, lightSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(lightEventListener);
    }

    /**
     * Detects is symbol is dot or dash.
     *
     * @param edgeTime  time of symbol duration
     * @return  character ( dot or dash )
     */
    public String detectSymbol(long edgeTime){
        long referenceEdgeTime = 300;  // It's 300ms
        float  multipleOfEdge = (float)edgeTime / (float)referenceEdgeTime;
        if (multipleOfEdge < 2){
            return ".";
        }
        return "-";
    }

    /**
     * The function detects time interval between symbols, words, letters.
     *
     * @param noEdgeTime  time without edge detection
     * @return  characters corresponding to given Morse time interval
     */
    public String detectSpace(long noEdgeTime){
        long referenceEdgeTime = 300;  // It's 300ms // same as in detectSymbol
        float  multipleOfNoEdge = (float)noEdgeTime / (float)referenceEdgeTime;
        if (0 < multipleOfNoEdge && multipleOfNoEdge < 2){
            return ""; // symbol space
        }
        if (2 < multipleOfNoEdge && multipleOfNoEdge < 5){
            return "ls";  // letter space
        }
        if (5 < multipleOfNoEdge && multipleOfNoEdge < 10.0) {
            return "ws";  // word space
        }
        return "N";
    }
}
